package com.thedude.evalandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void gotoMeteo(View v){
        Intent intent = new Intent(this,MeteoActivity.class);
        startActivity(intent);
    }


    public void gotoConverter(View v){
        Intent intent = new Intent(this,ConverterActivity.class);
        startActivity(intent);
    }
}
