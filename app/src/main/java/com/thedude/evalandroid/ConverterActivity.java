package com.thedude.evalandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;

public class ConverterActivity extends AppCompatActivity {

    public void convertToTTC(View view){
        EditText editTextHt = (EditText) findViewById(R.id.edtPrixHt);
        Double prixHt = Double.parseDouble(editTextHt.getText().toString());

        EditText editTextTva = (EditText) findViewById(R.id.edtTauxTva);
        double tauxTva = Double.parseDouble(editTextTva.getText().toString());

        double result = prixHt + prixHt * tauxTva/100 ;
        //conversion a 2 decimal
        BigDecimal bd = new BigDecimal(result);
        bd= bd.setScale(2,BigDecimal.ROUND_DOWN);
        result = bd.doubleValue();

        // ou
        //conversion a 2 decimal 2 eme methode
        //NumberFormat nf = new DecimalFormat("0.##");
        //String roundResult = nf.format(result);


        TextView textView = (TextView)findViewById(R.id.textResultat);
        textView.setText(Double.toString(result));

        // affichage en mode bulle
        //Toast.makeText(MainActivity.this, Double.toString(result), Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);
    }
}
